function pridat(event){
	var id1 = event.currentTarget.id;	
  var task = document.getElementById("add1").value;
  //console.log(task);
  if (task != ""){
   var todo = {id_user: id1, task: task, completed:false}
   //console.log(todo);
   $.ajax({
    type: 'POST',
    url: '/todo',
    data: todo,
    success: (data)=>{
          //do something with the data via front-end framework
          location.reload();
        }
      });
   return false;
 }else{

 }    
};

function filter(event){
   var filter = event.currentTarget.className.replace(/filter /g, "");
   //console.log(filter);
   $.ajax({
    type: 'GET',
    url: '/todo/' + filter,
    success: (data)=>{
          //do something with the data via front-end framework
          location.reload();
        }
      });
   return false;

};

function splnit(event){
	var id = event.currentTarget.className.replace(/splnit /g, "");
	$.ajax({
    type: 'PUT',
    url: '/todo/' + id,
    success: (data)=>{
          //do something with the data via front-end framework
          location.reload();
        }
      });
};

function upravit(event){  
  var id = event.currentTarget.className.replace(/upravit /g, "");
  var class1 = "task "+id;
  //console.log(class1);

  var task = document.getElementsByClassName(class1)[0];

  var disabled1 = document.getElementsByClassName(class1)[0].disabled;

  if(document.getElementsByClassName(class1)[0].value!=""){
    document.getElementsByClassName(class1)[0].disabled=!document.getElementsByClassName(class1)[0].disabled;
    if(document.getElementsByClassName(class1)[0].disabled==true){     
    //console.log("haha");
    $.ajax({
      type: 'PUT',
      url: '/todo/' +id+"-"+task.value,
      success: (data)=>{
          //do something with the data via front-end framework
          location.reload();
        }
      });
    }
  }else{
    
  }
};

function smazat(event){
	var task = event.currentTarget.className.replace(/smazat /g, "");
	$.ajax({
    type: 'DELETE',
    url: '/todo/' + task,
    success: (data)=>{
          //do something with the data via front-end framework
          location.reload();
        }
      });
};

function pridatEventy(){
	tlacitkaSp = document.getElementsByClassName("splnit");
	tlacitkaSm = document.getElementsByClassName("smazat");
  tlacitkaUp = document.getElementsByClassName("upravit");
  tlacitkaF = document.getElementsByClassName("filter");
  submit = document.getElementsByClassName("pridat");
  submit[0].addEventListener("click", pridat);
  for (i = 0; i < tlacitkaF.length; i++) {
    tlacitkaF[i].addEventListener("click",filter);
  }
  for (i = 0; i < tlacitkaSp.length; i++) {
    tlacitkaUp[i].addEventListener("click", upravit);
    tlacitkaSp[i].addEventListener("click", splnit);
    tlacitkaSm[i].addEventListener("click", smazat);
  };
};
document.addEventListener("DOMContentLoaded", pridatEventy);
