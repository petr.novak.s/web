var router = require("express").Router();
var bodyParser = require("body-parser");
var keys = require("../config/keys");
var Todo = require("../models/todo-model");
var User = require("../models/user-model");
var sanitize = require('mongo-sanitize');
var filter;

var urlencodedParser = bodyParser.urlencoded({extended: true});

const authCheck = (req,res,next)=>{
	if(!req.user){
		//user not logged in
		res.redirect("/");
	}else{
		next();
	}
};



//handling the routes


router.get("/todo/:filter",authCheck, (req, res)=>{
	//get data pass to view
	filter = sanitize(req.params.filter);
	//console.log(filter);
	if((filter == "true")||(filter=="false")){
		//console.log(filter);
		Todo.find({completed: filter}, (err, data)=>{
			if (err) {
				throw err;
			};
			//console.log(data);
			//vykreslení todo
			res.render('../views/todo',{todos: data, user: req.user, filter:filter});
		});
	}else{
		Todo.find({}, (err, data)=>{
			if (err) {
				throw err;
			};
			res.render('../views/todo',{todos: data, user: req.user, filter:filter});			
		});
	}	
});

router.get("/todo",authCheck, (req, res)=>{
	//get data pass to view
	//console.log(filter);
	if((filter == "true")||(filter=="false")){
		Todo.find({completed: filter}, (err, data)=>{
			if (err) {
				throw err;
			};
			//console.log(data);
			res.render('../views/todo',{todos: data, user: req.user, filter:filter});
			
		});
	}else{
		Todo.find({}, (err, data)=>{
			if (err) {
				throw err;
			};
			//console.log("nemá co dělat"+data);
			res.render('../views/todo',{todos: data, user: req.user, filter:filter});
			
		});

	}
	
});



router.get("/json",authCheck,(req,res)=>{		
	Todo.find({}, (err, data)=>{
		User.find({}, (err, data1)=>{
			if (err) {
				throw err;
			};
			if (err) {
				throw err;
			};
			res.render('../views/json-view',{todos: data, user: req.user,users: data1});
			
		});
});
});


//přidávání úkolů
router.post("/todo", urlencodedParser, (req,res)=>{
	//console.log(req.user.id);
	let tmp = sanitize(req.body);
	console.log(tmp);
	var newTodo = Todo(tmp).save((err, data)=>{
		if(err) throw err;		
		console.log("task added");
		res.json(data);
	})
	
});


//odstranění úkolů
router.delete("/todo/:task", (req,res)=>{
	var task = sanitize(req.params.task);
	Todo.findOne({_id: task}).deleteOne((err,data)=>{
		//console.log(req.params.task);
		if (err) throw err;
		res.json(data);
	});

});

//úprava úkolů WIP
router.put("/todo/:id-:task", (req,res)=>{
	//console.log();
	var id1 = sanitize(req.params.id);
	var task = sanitize(req.params.task);
	Todo.findOne({_id: id1},(err,data)=>{
		data.task = task;
		if (err) throw err;
		data.save();		
		res.json(data);			
	});
	

});

//upráva úkolů splněno/nesplněno
router.put("/todo/:id", (req,res)=>{
	var id1 = sanitize(req.params.id);
	Todo.findOne({_id: id1},(err,data)=>{
		var completed = sanitize(data.completed);
		if(data.completed=="false"){
			data.completed = "true";
		}else{
			data.completed = "false";
		}		 
		//console.log(data.completed);
		if (err) throw err;
		data.save();
		res.json(data);

			
	});

});

module.exports = router;
