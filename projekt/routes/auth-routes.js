var router = require("express").Router();
var passport = require("passport");

router.get("/logout",(req,res)=>{
	//passport
	req.logout();
	res.redirect("/");
});
//zavalá API na google 
router.get("/google", passport.authenticate("google",{
	scope: ['profile']
}));

//callback routa pro google
router.get("/google/redirect",  passport.authenticate("google"), (req,res)=>{	
	res.redirect("/todo");
});

module.exports = router;
