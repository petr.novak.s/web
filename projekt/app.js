var express = require('express');
var authRoutes = require("./routes/auth-routes");
var todoRoutes = require("./routes/todo-routes");
var passportSetup = require("./config/passport-setup");
var mongoose = require("mongoose");
var keys = require("./config/keys");
var cookieSession = require("cookie-session");
var passport = require("passport");
var bodyParser = require("body-parser");
var xssFilters = require('xss-filters');
var app = express();





//nastavení enginu
app.set('view engine', 'ejs');

app.use(cookieSession({
	maxAge: 60 * 60 * 1000,	//60 minut
	keys:[keys.session.cookieKey]
}));

//init passport
app.use(passport.initialize());
app.use(passport.session());

//db connect
mongoose.connect(keys.mongodb.dbURI,{useNewUrlParser: true },()=>{
	console.log("connected");
});
//setup routes
app.use("/auth",authRoutes);
app.use("/",todoRoutes);

//static files
app.use(express.static('./public'));

//vytvoření home route
app.get('/', (req,res)=>{
	res.render(xssFilters.inHTMLData("home"),{user: req.user});
});


//listen to port
let port = process.env.PORT;
if (port == null || port == "") {
  port = 3000;
}
app.listen(port);

