var mongoose = require("mongoose");


var todoSchema = new mongoose.Schema({		
	id_user: String,
	task: String,
	completed: String
});

const Todo = mongoose.model("Todo", todoSchema);

module.exports = Todo;