var passport = require("passport");
var GoogleStrategy = require("passport-google-oauth20");
var keys = require("./keys");
var User = require("../models/user-model");
var sanitize = require('mongo-sanitize');

//vezmu uživatele a vemu jeho id => připraveno pro cookie a poslání do prohlížeče
passport.serializeUser((user, done)=>{
	//místo null může být err
	done(null, user.id);
});

passport.use(
	new GoogleStrategy({
		//options strategy
		callbackURL:"/auth/google/redirect",
		//použití vygenerovaného API na google
		clientID: keys.google.clientID,
		clientSecret: keys.google.clientSecret
	},(accessToken, refreshToken, profile, done)=>{
		//kontrola uživatele jestli už není uložený v DB
 		var profileID = sanitize(profile.id);
		User.findOne({googleId: { $in: [profileID] }}).then((currentUser)=>{
			if(currentUser){				
				//console.log("user is:",currentUser);
				done(null, currentUser);
			}else{
			//vytvoření nového uživatle v DB
				new User({
					username: profile.displayName,
					googleId: profile.id
				}).save().then((newUser)=>{
					console.log("new user created "+newUser);
					done(null,newUser);
				});
			}
		});		
	})
	);


//vezmu id z cookie a najdu uživatele podle id
passport.deserializeUser((id, done)=>{
	//místo null může být err
	User.findById(id).then((user)=>{
		done(null, user);
	});	
});