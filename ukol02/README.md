### Bootstrap
[odkaz](https://github.com/twbs/bootstrap) <br>
Open source sada nástrojů pro vývoj responzivních webových aplikací v HTML, CSS a JS.<br>
Má nejvíce větví.<br>
### freeCodeCamp
[odkaz](https://github.com/freeCodeCamp/freeCodeCamp)<br>
Projekt (nezisková organizace), který učí uživatele programovat webové aplikace.<br>
Má nejvíce hvězdiček.<br>
### cleave.js 
[odkaz](https://github.com/nosir/cleave.js)<br>
Automatické formátování textu.<br>
Top trend tohoto týdne.<br>